This project introduces XML parser in Java. 

JAXP (Java API for XML Processing) provides three basic parsing interfaces: 
```
*DOM: the Document Object Model parsing interface
*SAX: the Simple API for XML parsing interface
*StAX: the Streaming API for XML or StAX interface (part of JDK 6; separate jar available for JDK 5)
```

JAXB (Java Architecture for XML Binding) allows Java developers to map Java classes to XML representations.

```
JDOM
Woodstox
XOM
dom4j
VTD-XML
Xerces-J
Crimson
```