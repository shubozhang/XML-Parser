package jaxb.XMLgenerator;


import jaxb.entity.Person;
import jaxb.entity.PersonList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class App {

    public static void main(String[] args) {
        generateXML(100);

        generateXML(10000);

        generateXML(1000000);
    }

    /**
     * It is used to generate sample xml file.
     * @param size: how many Person objects in sample xml file.
     */
    private static void generateXML(int size) {
        PersonList personList = new PersonList();
        for (int i = 0; i < size; i++) {
            String name = "A" + i;
            String id = name;
            Person p = new Person(name, id);
            personList.addPerson(p);
        }

        try {

            File file = new File("xml-files/" + size + ".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(PersonList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(personList, file);

            //jaxbMarshaller.marshal(personList, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }


}
